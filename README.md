Nested Krylov methods for shifted linear systems
------------------------------------------------

This code belongs to my [technical report] 'Nested Krylov methods for shifted linear systems' which is joint work with [Martin van Gijzen]. It provides two test cases:

+ wedge.m
+ elastic_wave.m

which are both Matlab drivers. They belong to the numerical experiments presented in Section 4.1 and Section 4.2, respectively. All parameters are set by default to the values described in the report.

Beneath the two test cases, the following multi-shift Krylov methods have been implemented:

| Function name    | method                                                    | Report section  |
| ---------------- |-----------------------------------------------------------| ---------------:|
| `msgmres.m`      | multi-shift GMRES without restart                         | 2.1             |
| `msqmridrs.m`    | multi-shift QMRIDR(s)                                     | 2.2             |
| `msfom.m`        | multi-shift FOM with a fixed number of (inner) iterations | 3.3.1           |
| `msidrs.m`       | multi-shift IDR(s) with collinear residuals               | 3.3.2           |
| `fmsgmres.m`     | flexible multi-shift GMRES                                | 3.4             |
| `fmsqmridrs.m`   | flexible multi-shift QMRIDR(s)                            | 3.5             |
| `msgmresk.m`     | restarted multi-shift GMRES, calls `msgmres_coll.m`       | See [Frommer]   |


The nested algorithms use `msfom.m` and `msidrs.m` as a flexible preconditioner in `fmsgmres.m` and `fmsqmridrs.m`, respectively.

[technical report]: http://www.ewi.tudelft.nl/en/the-faculty/departments/applied-mathematics/reports/
[Martin van Gijzen]: http://ta.twi.tudelft.nl/nw/users/gijzen/
[Frommer]: http://dl.acm.org/citation.cfm?id=277967
