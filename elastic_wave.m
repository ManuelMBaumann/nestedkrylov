%%
%
%  driver for elastic wave equation
%
%%

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%

clear all
close all
addpath('discretization/')
addpath('solvers/')
addpath('aux')

Title = 'Elastic Wave Equation';
exper = 0;
c = ['b','g','r','c','m','y','k'];

%%  Default parameter settings                                             
L  = 1;                                 % length
h  = 0.01;                              % discretization size
f  = 5000:5000:30000;                   % frequencies         

om = 2*pi*f;                            % angular frequencies   
 
E = 7e10;                               % Young modulus 
ni = 0.33;                              % Poisson ration
delta = 2700;                           % Material density

sommerfeld = 1;                         % flag for BC

lame1 = E./(2*(1+ni));                  % Lame param \lambda
lame2 = E.*ni./((1+ni).*(1-2*ni));      % Lame param \mu
cp = sqrt((lame1 + 2*lame2)./delta);    % speed of pressure wave
cs = sqrt(lame2./delta);                % speed of shear wave
                                       
h_rec = min((cs*2*pi./om)./10);


%%  User input
prompt = {'L', ['h, recommended: ' num2str(h_rec)], ...
    'E', 'ni', 'delta', 'sommerfeld (0/1)'};
defaults = {num2str(L), num2str(h), ...
    num2str(E), num2str(ni), num2str(delta), num2str(sommerfeld)};
lineNo=[1 25];
params=inputdlg(prompt,Title,lineNo,defaults);

% parsing
L          = str2num(char(params(1)));
h          = str2num(char(params(2)));
E          = str2num(char(params(3)));
ni         = str2num(char(params(4)));
delta      = str2num(char(params(5)));
sommerfeld = str2num(char(params(6)));

om = 2*pi*f;                            % angular frequencies (update)
lame1 = E./(2*(1+ni));                  % Lame param \lambda
lame2 = E.*ni./((1+ni).*(1-2*ni));      % Lame param \mu
cp = sqrt((lame1 + 2*lame2)./delta);    % speed of pressure wave
cs = sqrt(lame2./delta);                % speed of shear wave

m = round(L/h)+1;   % guarantee m is a natural number 
h = L/(m-1);        % correct discretization size
n = m^2;            % # nodes

% default solver parameters:
maxit = 1000;
tol   = 1e-8;
im    = sqrt(-1);
s     = 4;
sh    = 0.7-0.7*im;
inner = 25;
outer_flag = 1;
inner_flag = 0;
plotInner = 0;

%%  Generate discretization matrices
K = buildK(m,lame1,lame2);
M = buildM_lumped(m,h,delta);
if sommerfeld
  C = buildC(1,1,1,1,m,h,cp,cs,delta);
else
  C = [];
end

% Right-hand side vector (point source in middle)
mp = ceil(m/2)*m - ceil(m/2);
rhsUx = zeros(n,1);
rhsUy = zeros(n,1);
rhsUx(mp) = 1;
rhsUy(mp) = 1;
rhs = [rhsUx;rhsUy];
%%

contin = 1;
while contin

   exper = exper + 1;

   solver_parameters;

   t0 = cputime;
   [x, iter, resvec,relres,method] = precon_ms(K,C,M,rhs,tol,maxit,f,sh, s,inner,plotInner,...
       inner_flag,outer_flag);
   time = cputime-t0;
   
   disp(['Experiment ', num2str(exper)]);
   disp('====================');

   output;

end

% extract solution
Ux = real(x(1:n,:));
Uy = real(x(n+1:2*n,:));

%%  plot solution
plot_elastic(L,m,Ux,Uy,f);
