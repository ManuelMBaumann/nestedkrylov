%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%


c = ['b','g','r','c','m','y','k'];
%
%    Output:
disp(' ');
disp('Solver parameters.')
disp(['Solver is ',method])
disp(['Number of inner iteration = ',num2str(inner)]);
disp(['Shift of preconditioner = ',num2str(sh)]);

if sh == 0
   disp('No preconditioner');
else
   disp('Exact shifted-Laplace preconditioner');
end
disp(' ');
disp('Results.');
disp(['Iterations: ',num2str(iter)]);
disp(['CPU time: ',num2str(time),'s.']);
disp('Accuracy: ')
for j = 1:length(f)
   disp(['Frequency = ',num2str(f(j)),', |b-Ax|/|b| = ',num2str(relres(j))]);
end
disp(' ');

scrsz = get(0,'ScreenSize');
figure('Position',[scrsz(1) + 2*scrsz(3)/3 scrsz(4)/2 scrsz(3)/3 scrsz(4)/2]);
xlabel('Number of matrix-vector multiplications');
ylabel('Relative residual norm [log]');
title('Convergence');

grid on;
hold on;
x_as = [0:1:iter];
k = 0;
frequency = [];
for j = 1:length(f)
   k = k + 1;
   if ( k > 7 ) k = 1; end;
   plot(x_as,log10(resvec(:,j)/resvec(1,j)),[c(k),'-']);
   frequency = strvcat( frequency,['f = ',num2str(f(j))]);
end
legend(frequency);
hold off;

%  save_pgfplot(iter,resvec,['f1';'f2';'f3';'f4';'f5';'f6'],'resvec')

