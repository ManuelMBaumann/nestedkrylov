%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%


Title = 'Solver parameters';
prompt = {'Frequencies', ...
          'Shift of preconditioner', ...
          'Outer method [1=GMRES, 2=GMRES(k), 3=QMRIDR, 4=collIDR]', ...
          'Outer iterations', ...
          'Inner method [0=none, 1=FOM, 2=collIDR]', ...
          'Inner iterations', ...
          's (if IDR is used)', ...
          'Tolerance', ...
          'Display inner convergence (0/1)'};
defaults = {num2str(f), num2str(sh), num2str(outer_flag), num2str(maxit), num2str(inner_flag), ...
    num2str(inner), num2str(s), num2str(tol), num2str(plotInner)};
lineNo=[1 60];
params=inputdlg(prompt,Title,lineNo,defaults);
if ( isempty(params) ) contin = 0; break; end;
%
% Actual values:
f = str2num(char(params(1)));
sh = str2num(char(params(2)));
outer_flag = str2num(char(params(3)));
maxit = str2num(char(params(4)));
inner_flag = str2num(char(params(5)));
inner = str2num(char(params(6)));
if ( isempty(inner) ) inner = 0; end;
s = str2num(char(params(7)));
tol = str2num(char(params(8)));
plotInner = str2num(char(params(9)));