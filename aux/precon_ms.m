function [x,iter,resvec,relres,method]=precon_ms(K,C,M,rhs,tol,maxit,f,sh, s,inner_iter,plotInner,inner_flag,outer_flag)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%

%% Build shifted system
damping = ~isempty(C);
n = length(rhs);
im = sqrt(-1);
if damping
%     Block system 
   sigma = 2*pi*f;
   seed  = sigma(end);
   shift = sh*seed;
   I = speye(n);
   O = sparse(n,n);
   b = zeros(2*n,1);
   b(1:n) = rhs;
   sc = trace(K)/n;
   A = [(sc*im)*C K; sc*I O];
   B = [     sc*M O;    O I];
else
   sigma = (2*pi)^2*f.^2;
   seed  = sigma(end);
   shift = sh*seed;
   I = speye(n);
   A = K;
   B = M;
   b = rhs;
end

%% Make seed system:   
A = A - seed*B;
sigma = sigma - seed;
shift = shift - seed;

n_sigma = length(sigma);
ritzval = [];
MAT.name = 'mv';
MAT.sh = sh;

if ( sh == 0 ) %no preconditioner
   eta = sigma;
   MAT.A = A;
   MAT.B = B;
else
   [L,U,P,Q,R] = lu(A-shift*B);
   eta = sigma./(sigma-shift);
%
%        Store information about the system:
   MAT.A = A;
   MAT.B = B;
   MAT.L = L;
   MAT.U = U;
   MAT.P = P;
   MAT.Q = Q;
   MAT.R = R;
end


%% Solve the systems:

if outer_flag == 1
    if inner_flag == 1 || inner_flag ==2
        if inner_flag == 1
            method = 'FOM-FGMRES';
        else
            method = 'IDR-FGMRES';
        end
        [y, iter, resvec]   = fmsgmres(MAT,b,eta,inner_iter,tol,maxit,plotInner,inner_flag);
    else
        method = 'msGMRES';
        [y, iter, resvec,~] = msgmres(MAT,b,eta,tol,maxit);
    end
        
    
elseif outer_flag == 2
    method = 'rest_GMRES';
    [y, iter, resvec] = msgmresk(MAT,b,eta,maxit, inner_iter);
    
    
elseif outer_flag == 3
    if inner_flag == 1 || inner_flag ==2
        if inner_flag == 1
            method = 'FOM-FQMRIDR';
        else
            method = 'IDR-FQMRIDR';
        end
        [y,~,~,iter,resvec] = fmsqmridrs(MAT,b,eta,s,inner_iter,tol,maxit,plotInner,inner_flag);
    else
        method = 'msQMRIDR';
        [y,~,~,iter,resvec] = msqmridr(MAT,b,eta,s,tol,maxit);
    end
    
    
elseif outer_flag == 4
    method = 'coll_IDR';
    [y,~,iter,resvec] = msidrs(MAT,b,eta,s,tol,maxit );
    
    
else
    disp('Choose outer flag in {1,2,3,4}.')
end


%%

% Scale the solution back
x = zeros(length(b),n_sigma);

if ( sh == 0 )
   x = B\y;
else
   y = Q*(U\(L\(P*(R\y))));
   for j = 1:n_sigma
      x(:,j) = y(:,j)*(1-eta(j));
   end
end

if ( damping )
   x = x(n+1:2*n,:);
end


% Calculate the relative residual norms of the original system
sigma = sigma + seed;
normb = norm(rhs);
relres = zeros(n_sigma,1);
for j = 1:length(f)
   if ( damping )
      xs = x(:,j);
      r = rhs - (K*xs + sigma(j)*im*(C*xs) - sigma(j)^2*(M*xs));
      relres(j) = norm(r)/normb;
   else
      xs = x(:,j);
      r = rhs - (K*xs - sigma(j)*(M*xs));
      relres(j) = norm(r)/normb;
   end
end

return

