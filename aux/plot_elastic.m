function plot_elastic(L,m,Ux_all,Uy_all,f)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%

nf = length(f);

% the square mesh
x1 = linspace(0,L,m);
X1 = kron(ones(m,1),x1);
X2 = X1';
figure;
hold on;
for ii = 1:min(nf,6)
    Ux = Ux_all(:,ii);
    Uy = Uy_all(:,ii);
    
    subplot(2,3,ii)
    surf(X1,X2,flipud(reshape(sqrt(Ux.^2 + Uy.^2),m,m)')); 
    title(['f = ', num2str(f(ii))] ); 
    cmax = max(sqrt(Ux.^2 + Uy.^2)); cmin =  min(sqrt(Ux.^2 + Uy.^2));
    colorbar; 
    caxis([cmin cmax/3]);
    view(90,270)
    shading interp

end
end
