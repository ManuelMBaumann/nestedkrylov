function plot_wedge(dm,x,f)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%

for i_sigma = 1:length(f)
   y = real(x);
   cmin = min(y); cmax = max(y);
   y = flipud(reshape(real(x(:,i_sigma)), dm )');
   figure;
   pcolor(y);
   title(['f = ', num2str(f(i_sigma))] );
   colorbar;
   caxis([cmin(i_sigma)/3 cmax(i_sigma)/3]);
   axis equal
   shading interp;
end