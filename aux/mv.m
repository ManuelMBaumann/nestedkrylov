function x = mv( y, MAT )

%
% Function for (preconditioned) matrix-vector multiplication.
%
% The software is distributed without any warranty.
%
% Martin van Gijzen
% Copyright (c) December 2010
%

if MAT.sh == 0
   x = MAT.A*(MAT.B\y);
else   
   x = MAT.Q*(MAT.U\(MAT.L\(MAT.P*(MAT.R\y))));
   x = MAT.A*x;
end

end
