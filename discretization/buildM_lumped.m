function M = buildM_lumped(m,h,delta)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) October 2014
%

% Lumped mass matrix.


n = m*m;

D = speye(m,m);
D(1,1) = 0.5;
D(m,m) = 0.5;

On  = sparse(n,n);

M11 = h^2*kron(D, D);
M11(1,1) = M11(1,1)*(2/3); M11(m,m) = M11(m,m)*(4/3);
M11(n-m+1,n-m+1) = M11(n-m+1,n-m+1) *(4/3); M11(n,n) = M11(n,n)*(2/3);

M22 = M11;

M = delta*[M11 On;
              On M22];

end