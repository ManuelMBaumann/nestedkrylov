function K = buildK(m,lame1,lame2)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) October 2014
%

% Stiffness matrix.


T = gallery('tridiag',m,-1,2,-1);
T(1,1) = 1;
T(m,m) = 1;
D = speye(m,m);
D(1,1) = 0.5;
D(m,m) = 0.5;


Diffx = kron(D,T);
Diffy = kron(T,D);
Lap   = Diffx + Diffy;


TT = gallery('tridiag',m,0.5,-1,0.5);
TT(1,1) = -0.5;
TT(m,m) = -0.5;

TTT = gallery('tridiag',m,0,0.5,-0.5);
TTT(1,1)=0;

TTTT = gallery('tridiag',m,-0.5,0.5,0);
TTTT(end,end)=0;

Diffxy = kron(speye(m),TT) + kron(gallery('tridiag',m,0,0,1),TTT) + kron(gallery('tridiag',m,1,0,0),TTTT);
Diffxy(1:m,1:m) = gallery('tridiag',m,0,-0.5,0.5);
Diffxy(end-m+1:end,end-m+1:end) = gallery('tridiag',m,0.5,-0.5,0);
Diffxy(1,1)=0; Diffxy(end,end)=0;


% Stiffness matrix:
K = [lame2*Lap+(lame1+lame2)*Diffx (lame1+lame2)*Diffxy;
     (lame1+lame2)*Diffxy' lame2*Lap+(lame1+lame2)*Diffy];

end

