function C = buildC(N,E,S,W,m,h,cp,cs,delta)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) October 2014
%

% Inlcude Sommerfeld impedance boundary conditions.
% Absorbing boundary conditions if [N,E,S,W] == 1, else reflection.

n = m^2;
gamma = 1; % absorbing BC

C1 = spalloc(n,n,3*m);
C2 = spalloc(n,n,3*m);
O  = sparse(n,n);

T = h*gallery('tridiag',m,1/6,2/3,1/6);
T(1,1) = 0.5*T(1,1); T(m,m) = 0.5*T(m,m);

if N == 1
    
   C1(end-m+1:end,end-m+1:end) = cs*T;
   C2(end-m+1:end,end-m+1:end) = cp*T;  
   
end

if E == 1
    
    C1(m,m)   = cp*(1/3)*h;
    C1(m,m+m) = cp*(1/6)*h;
    
    C2(m,m)   = cs*(1/3)*h;
    C2(m,m+m) = cs*(1/6)*h;
    for mm = 2:m-1
        ind = mm*m;
        
        C1(ind,ind+m) = cp*(1/6)*h;
        C1(ind,ind)   = cp*(2/3)*h;
        C1(ind,ind-m) = cp*(1/6)*h;
        
        C2(ind,ind+m) = cs*(1/6)*h;
        C2(ind,ind)   = cs*(2/3)*h;
        C2(ind,ind-m) = cs*(1/6)*h;
    end
    C1(end,end)   = cp*(1/3)*h;
    C1(end,end-m) = cp*(1/6)*h;
    
    C2(end,end)   = cs*(1/3)*h;
    C2(end,end-m) = cs*(1/6)*h;
end

if S == 1
    
   C1(1:m,1:m) = cs*T;
   C2(1:m,1:m) = cp*T;  
   
end

if W == 1
    
    C1(1,1)   = cp*(1/3)*h;
    C1(1,1+m) = cp*(1/6)*h;
    
    C2(1,1)   = cs*(1/3)*h;
    C2(1,1+m) = cs*(1/6)*h;
    for mm = 2:m-1
        ind = (mm-1)*m + 1;
        
        C1(ind,ind+m) = cp*(1/6)*h;
        C1(ind,ind)   = cp*(2/3)*h;
        C1(ind,ind-m) = cp*(1/6)*h;
        
        C2(ind,ind+m) = cs*(1/6)*h;
        C2(ind,ind)   = cs*(2/3)*h;
        C2(ind,ind-m) = cs*(1/6)*h;
    end
    C1(end-m+1,end-m+1)   = cp*(1/3)*h;
    C1(end-m+1,end-2*m+1) = cp*(1/6)*h;
    
    C2(end-m+1,end-m+1)   = cs*(1/3)*h;
    C2(end-m+1,end-2*m+1) = cs*(1/6)*h;
    
end

C = gamma*delta*[C1 O;
                 O C2];

end