function [x_s,gamma] = msfom(A,b,sigma,iter)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%

% Multi-shift FOM


funA = 0;
if isa(A,'struct')
   funA = 1;
   if isfield(A,'name')
      function_A = A.name;
   else
      error('Use field A.name to specify function name for matrix-vector multiplication');
   end
end

n       = length(b); 
n_sigma = length(sigma);

beta = norm(b);

v = b/beta;
V = zeros(n,iter);
H = zeros(iter,iter);

for k = 1:iter

   V(:,k) = v;
%  Compute new basis vector
   if funA
      v = feval( function_A, v, A);
   else
      v = A*v;
   end
%
%  Orthogonalise the new basis vector 
   for j = 1 : k
      H(j,k) = dot(V(:,j),v);
      v = v - H(j,k) * V(:,j);
   end
   h = norm(v);
   v = v/h;
   if ( k < iter )
      H(k+1,k) = h;
   end

end
%
%  Solve shifted systems
e1  = zeros(iter,1); e1(1)=beta; 
I   = eye(iter,iter);
y_s = zeros(iter,n_sigma);
for i_sigma = n_sigma:-1:1
   H_s = H-sigma(i_sigma)*I;
   y_s(:,i_sigma) =H_s\e1;
   gamma(i_sigma) = y_s(iter,i_sigma)/y_s(iter,n_sigma);
end
x_s = V*y_s; 

end
