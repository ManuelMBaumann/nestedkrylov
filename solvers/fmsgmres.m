function [x,iter, resnrm] = fmsgmres(A,b,sigma,inner_maxit,tol,maxit,plotInner,inner_flag)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%

% Flexible multi-shift GMRES

funA = 0;
if isa(A,'struct')
   funA = 1;
   if isfield(A,'name')
      function_A = A.name;
   else
      error('Use field A.name to specify function name for matrix-vector multiplication');
   end
end

n       = length(b); 
n_sigma = length(sigma);
x       = zeros(n,n_sigma);
resnrm  = zeros(maxit+1,n_sigma);

beta        = norm(b);
resnrm(1,:) = beta;

v  = b/beta;
V  = zeros(n,maxit);
Z  = zeros(n,maxit);
H  = zeros(maxit+1,maxit);
Zs = zeros(n,maxit,n_sigma);
Hs = zeros(maxit+1,maxit,n_sigma);
converged = zeros(n_sigma,1);

for k = 1:maxit  
   
%  Compute new basis vector
   V(:,k) = v;
   
   if ~plotInner
       
       if inner_flag == 1
           [zs, gamma] = msfom( A, v, sigma, inner_maxit );
       else
           [zs, gamma]=msidrs(A,v,sigma,4,1e-1,inner_maxit); 
       end
       
   else
       
       if inner_flag == 1
           [zs,gamma,inner_iter,inner_resn] = msfom2(A,v,sigma,inner_maxit,1e-1);
       else
           [zs, gamma, inner_iter,inner_resn]=msidrs(A,v,sigma,4,1e-1,inner_maxit); 
       end
         
       col = ['b','g','r','c','m','y','k'];
       figure(1000+k)
       xlabel('Number of matrix-vector multiplications');
       ylabel('Relative residual norm');
       title('Inner convergence');

       grid on;
       hold on;
       kk = 0;
       for j = 1:n_sigma
           kk = kk + 1;
           if ( kk > 7 ) kk = 1; end
           plot(0:1:inner_iter,log10(inner_resn(:,j)),[col(kk),'-']);   
       end
       hold off;
   end
   
   z = zs(:,n_sigma);
   
   if funA
      v = feval( function_A, z, A);
   else
      v = A*z;
   end
%
%  Orthogonalise the new basis vector 
   for j = 1 : k
      H(j,k) = dot(v,V(:,j));
      v = v - H(j,k) * V(:,j);
   end
   H(k+1,k) = norm(v);
   v = v/H(k+1,k);
   Z(:,k) = z;
   for i_sigma = 1:n_sigma
      Zs(:,k,i_sigma) = zs(:,i_sigma);
   end
%
%  Solve small shifted system
   e1 = zeros(k+1,1); e1(1)=beta;
   
   y = zeros(k,n_sigma);
   for i_sigma = 1:n_sigma
      if ( ~converged(i_sigma) )
         Hs(:,k,i_sigma) = gamma(i_sigma)*H(:,k);
         Hs(k,k,i_sigma) = Hs(k,k,i_sigma) - (gamma(i_sigma)-1);
         Ht = Hs(1:k+1,1:k,i_sigma);
         y(:,i_sigma) =Ht\e1;
         resnrm(k+1,i_sigma) = norm(e1 - Ht*y(:,i_sigma));
         if resnrm(k+1,i_sigma) < tol*beta
            converged(i_sigma) = 1;
            x(:,i_sigma) = Zs(:,1:k,i_sigma)*y(:,i_sigma);
         end
      end
   end

   if converged
      break
   end
end
iter = k;
resnrm = resnrm(1:iter+1,:);

for i_sigma = 1:n_sigma
   if ( ~converged(i_sigma) )
      x(:,i_sigma) = Zs(:,1:iter,i_sigma)*y(:,i_sigma);
   end
end

end
