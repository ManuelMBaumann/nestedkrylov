function [x_s,gamma,iter,res_vec] = msfom2(A,b,sigma,maxit,tol)


funA = 0;
if isa(A,'struct')
   funA = 1;
   if isfield(A,'name')
      function_A = A.name;
   else
      error('Use field A.name to specify function name for matrix-vector multiplication');
   end
end

n        = length(b); 
n_sigma  = length(sigma);
gamma    = zeros(n_sigma,1);

beta = norm(b);

res_vec          = zeros(maxit+1,n_sigma);
res_vec(1,:)     = beta*ones(1,n_sigma);

v = b/beta;
V = zeros(n,maxit);
H = zeros(maxit+1,maxit);

for k = 1:maxit

   V(:,k) = v;
%  Compute new basis vector
   if funA
       v = feval( function_A, v, A);
   else
       v = A*v;
   end
%
%  Orthogonalise the new basis vector 
   for j = 1 : k
      H(j,k) = dot(V(:,j),v);
      v = v - H(j,k) * V(:,j);
   end
   h = norm(v);
   v = v/h;
   
   H(k+1,k) = h;
   
   
    %  Solve system
    e1 = zeros(k,1); e1(1)=beta; 
    Hk = H(1:k,1:k);
    Vk = V(:,1:k);
    
%     y = Hk\e1;
%     x = Vk*y;
    %
    %  Solve shifted systems
    I = eye(k,k);
    y_s = zeros(k,n_sigma);
    for i_sigma = n_sigma:-1:1
       H_s = Hk-sigma(i_sigma)*I;
       y_s(:,i_sigma) =H_s\e1;
       gamma(i_sigma) = y_s(end,i_sigma)/y_s(end,n_sigma);
    end
    x_s = Vk*y_s; 
   
%     k
%     for i_sigma = 1:n_sigma
%        (b-(A-sigma*eye(n,n))*x_s(:,i_sigma))./(b-A*x)
%     end
    
    

    normr = norm(-H(k+1,k)*y_s(end,n_sigma)*v);
    %normr = max(abs(gamma))*normr;
    
    res_vec(k+1,:) = abs(gamma)*normr;
    normr = max(res_vec(k+1,:));
    
    if normr < tol
        iter = k;
        res_vec=res_vec(1:k+1,:);
        break
    end
   
   
end
iter = k;


end