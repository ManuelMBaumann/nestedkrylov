function [x_s,coll_fac,iter,resnrm] = msidrs(A,b,sigma,s,tol,maxit )

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%

% Multi-shift IDR(s) with collinear residuals


%% Set all parameters and allocate fields
funA = 0;
if isa(A,'struct')
   funA = 1;
   if isfield(A,'name')
      function_A = A.name;
   else
      error('Use field A.name to specify function name for matrix-vector multiplication');
   end
end

n       = length(b);
n_sigma = length(sigma);
randn('state', 0);
P = randn(n,s);
P = orth(P);
angle = 0.7;

x_s  = zeros(n,n_sigma);
dX_s = zeros(n,s,n_sigma);

% Take transpose of P for efficiency reasons
P = P';

% Compute initial residual:
r = b;
tolb = tol * norm(b);        % Relative tolerance

normr       = norm(r);
resnrm      = zeros(maxit+s+1,n_sigma);
resnrm(1,:) = normr*ones(1,n_sigma);

D    = gallery('tridiag',zeros(s,1),ones(s+1,1),-ones(s,1));

G = zeros(n,s+1);

beta          = zeros(s+1,n_sigma);
beta(s+1,:)   = ones(1,n_sigma);
om_s          = zeros(n_sigma,1);
c_s           = zeros(s+1,n_sigma);
alpha_s       = zeros(1,n_sigma);
converged     = zeros(n_sigma,1);


%---------------- Compute starting vectors: ------------
iter = 0;
om = 1;
m = P*r;
G(:,s+1) = r;

for k = s:-1:1

   iter = iter + 1;

   if funA
      v = feval( function_A, r, A);
   else
      v = A*r;
   end
   om = omega( v, r, angle );
   G(:,k) = r-om*v;

%% shifted systems
   for i_sigma = 1:n_sigma

      beta(k,i_sigma)   = beta(k+1,i_sigma)/(1-om*sigma(i_sigma));
      om_s(i_sigma)     = om/(1-om*sigma(i_sigma));

      dX_s(:,k,i_sigma) = om_s(i_sigma)*beta(k+1,i_sigma)*r;
      x_s(:,i_sigma)    = x_s(:,i_sigma) + dX_s(:,k,i_sigma);

   end

   r = G(:,k);
   normr = norm(r);
   m_old = m;
   m = P*r;
   M(:,k) = m-m_old;

   for i_sigma = 1:n_sigma
      resnrm(iter+1,i_sigma) = abs(beta(k,i_sigma))*normr;
      converged(i_sigma) = ( resnrm(iter+1,i_sigma) < tolb );
   end 
%%

end

% Main iteration loop, build G-spaces:

while ( sum(converged) < n_sigma & iter < maxit )  

   for k = 0:s
      c = M\m; 
      v = G*D*[1;c];

% shifted
      for i_sigma = 1:n_sigma
         coll = diag(beta(:,i_sigma));
         c_s(:,i_sigma)   = (coll*D)\(D*[1;c]);
         alpha_s(i_sigma)   = 1/c_s(1,i_sigma);
         c_s(:,i_sigma) = alpha_s(i_sigma)*c_s(:,i_sigma);
      end

      if funA
         t = feval( function_A, v, A);
      else
         t = A*v;
      end

%        Initialisation new G-space?
      if ( k == 0 ) 
         om = omega( t, v, angle );
         for i_sigma = 1:n_sigma
             om_s(i_sigma)   = om/(1-om*sigma(i_sigma));
         end
      end 
      r = v - om*t; 

      iter = iter + 1;

      normr = norm(r);

      m_old = m;
      m = P*r;
      dm = m - m_old;
      M(:,2:s) = M(:,1:s-1); M(:,1) = dm;

      for i_sigma = 1:n_sigma
         beta(2:s+1,i_sigma) = beta(1:s,i_sigma);
         beta(1,i_sigma) =  alpha_s(i_sigma)/(1-om*sigma(i_sigma));

         dx_s = -dX_s(:,:,i_sigma)*c_s(2:s+1,i_sigma) + (om_s(i_sigma)*alpha_s(i_sigma))*v;
         x_s(:,i_sigma) = x_s(:,i_sigma) + dx_s;

% IDR update
         dX_s(:,:,i_sigma) = [dx_s dX_s(:,1:s-1,i_sigma)];
         resnrm(iter+1,i_sigma) = abs(beta(1,i_sigma))*normr;
         converged(i_sigma) =  (resnrm(iter+1,i_sigma) < tolb);
      end

      G = [r G(:,1:s)];
   end; % k = 0:s
end; %while

coll_fac = beta(1,:);
resnrm = resnrm(1:iter+1,:);

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function om = omega( t, s, angle )

ns = norm(s);
nt = norm(t);
ts = dot(t,s);
rho = abs(ts/(nt*ns));
if ( rho < 1e-12 )
   om = ns/nt;
else
   om=ts/(nt*nt);
   if ( abs(rho ) < angle )
      om = om*angle/abs(rho);
   end
end

return
