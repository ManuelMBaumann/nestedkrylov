function [x, flag, relres, iter, resvec]=fmsqmridrs(A,b,sigma,s,inner_iter,tol,maxit,plotInner,inner_flag,options)

%   The software is distributed without any warranty.
%
%   Martin van Gijzen
%   Copyright (c) July 2014
%

% Flexible multi-shift QMRIDR

if ( nargout == 0 )
   help msqmridr;
   return
end

% Check for an acceptable number of input arguments
if nargin < 2
   error('Not enough input arguments.');
end

% Check matrix and right hand side vector inputs have appropriate sizes
funA = 0;
if isa(A,'struct')
   funA = 1;
   if isfield(A,'name')
      function_A = A.name;
   else
      error('Use field A.name to specify function name for matrix-vector multiplication');
   end
   n = length(b);
else
   [m,n] = size(A);
   if (m ~= n)
      error('Matrix must be square.');
   end
   if ~isequal(size(b),[m,1])
      es = sprintf(['Right hand side must be a column vector of' ...
            ' length %d to match the coefficient matrix.'],m);
      error(es);
   end
end

% Assign default values to unspecified parameters
if nargin < 3 || isempty(sigma)
   sigma = 0;
end
if nargin < 4 || isempty(s)
   s = 4;
end
if ( s > n )
   s = n;
end

if nargin < 5 || isempty(inner_iter)
   inner_iter = 10;
end
if nargin < 6 || isempty(tol)
   tol = 1e-8;
end
if nargin < 7 || isempty(maxit)
   maxit = min(2*n,1000);
end
if nargin < 8 || isempty(plotInner)
   plotInner = 0;
end
if nargin < 9 || isempty(inner_flag)
   inner_flag = 0;
end


% Other parameters
randn('state', 0);
P = randn(n,s);
P = orth(P);
trueres = 0;
angle = 0.7;

if ( nargin > 9  )
% Computation of mu:
   if isfield(options,'mu')
      angle = options.mu;
   end
% Alternative definition of P:
   if isfield(options,'P')
      P = options.P;
      if ~isequal(size(P),[n,s])
          es = sprintf(['P must be a matrix of' ...
          ' size %d times %d to match the problem size.'],n,s);
          error(es);
      end
   end
   if isfield(options,'resnorm')
      trueres = options.resnorm == 1;
   end
end


% END CHECKING INPUT PARAMETERS AND SETTING DEFAULTS

n_sigma = length(sigma);
x = zeros(n,n_sigma);
% Check for zero rhs:
if (norm(b) == 0)            % Solution is nulvector
   iter = 0;
   resvec = zeros(1,n_sigma);
   flag = 0;
   relres = zeros(1,n_sigma);
   return
end

% Initialize output paramater relres
relres = ones(1,n_sigma);

% Compute initial residual:
normb = norm(b);
tolb = tol * normb;          % Relative tolerance

resvec = zeros(maxit,n_sigma);
M = zeros(s,s);
G = zeros(n,s);
g = b/normb;
normr = normb*ones(1,n_sigma);
resvec(1,:) = normr;

if (max(normr) <= tolb)      % Initial guess is a good enough solution
   iter = 0;
   flag = 0;
   relres = normr/normb;
   return
end

W = zeros(n,s+1,n_sigma);
w = zeros(n,1);
v      = zeros(n,1); 
Vtilde = zeros(n,n_sigma);
vtilde = zeros(n,1); 

% Last two nonzero coefficients of projected rhs:
phi_n =  zeros(n_sigma);
phi_n1 = zeros(n_sigma);
phi_n1 = normr;

iter = 0;
cs = zeros(s+2,n_sigma);
sn = zeros(s+2,n_sigma);

mu = 0;
j = 0;
converged = zeros(n_sigma,1);
inner_count =1;
while ( max(normr) > tolb && iter < maxit )

   for k = 1:s+1
%
% Update counters:
      iter = iter + 1;
%
% First phase: make new vector in G-space:
      c = zeros(s+2,1); c(s+1) = 1;
      m = P'*g; 
      if iter > s
%
% Construct v orthogonal to P
         gamma = M\m;
         v = g - G*gamma;
	     c(1:s) = -gamma;
      else  
%
% First s steps: Arnoldi
         v = g;
      end
      M(:,1:s-1) = M(:,2:s); M(:,s) = m;
      G(:,1:s-1) = G(:,2:s); G(:,s) = g; 
%
% Flexible preconditioning
      if ~plotInner
          
          if inner_flag == 1
              [Vtilde, eta] = msfom( A, v, sigma, inner_iter );
          else
              [Vtilde, eta]=msidrs(A,v,sigma,s,1e-1,inner_iter); 
          end
           
      else
          
          if inner_flag == 1
              [Vtilde, eta, inner_iter,inner_resn] = msfom( A, v, sigma, inner_iter );
          else
              [Vtilde, eta, inner_iter,inner_resn]=msidrs(A,v,sigma,s,1e-1,inner_iter);
          end
              
          col = ['b','g','r','c','m','y','k'];
          figure(1000+inner_count)
          xlabel('Number of matrix-vector multiplications');
          ylabel('Relative residual norm');
          title('Inner convergence');

          grid on;
          hold on;
          kk = 0;
          for j = 1:n_sigma
             kk = kk + 1;
             if ( kk > 7 ) kk = 1; end
             plot(0:1:inner_iter,log10(inner_resn(:,j)),[col(kk),'-']);   
          end
          hold off;
          inner_count = inner_count + 1;
          
      end

      vtilde = Vtilde(:,n_sigma);
%
% Compute new vector in space G_j
      if ( funA )
         g = feval( function_A,vtilde,A );
      else
         g = A*vtilde;
      end
%
% New G-space? 
      if k == s+1
         mu =  compmu( g, v, angle );
	 j = j + 1;
      end
      g = g - mu*v;
%
% Orthogonalisation (2 times classical Gram-Schmidt)
      h = mu*c;
      if ( k < s+1 )
         beta  = G(:,s-k+1:s)'*g; g = g - G(:,s-k+1:s)* beta;
         cbeta = G(:,s-k+1:s)'*g; g = g - G(:,s-k+1:s)*cbeta;
         beta = beta + cbeta;
         h(s+1-k+1:s+1) = h(s+1-k+1:s+1)+beta;
      end
%
% Normalise
      normg = norm(g);
      g = g/normg;
      h(s+2) = normg;
%
% Store the information
      for k_sigma = 1:n_sigma
         if ~converged(k_sigma);
            r_sigma = zeros(s+3,1);
            r_sigma(2:s+3) = eta(k_sigma)*h - (eta(k_sigma)-1)*c;
%
            low = max(1,s+3-iter);
            for l = low:s+1,
%
% Apply Givens rotation.
               t = r_sigma(l); 
               r_sigma(l)   =       cs(l,k_sigma) *t + sn(l,k_sigma)*r_sigma(l+1);
               r_sigma(l+1) = -conj(sn(l,k_sigma))*t + cs(l,k_sigma)*r_sigma(l+1);
            end
%
% Form i-th rotation matrix.
            [cs(s+2,k_sigma) sn(s+2,k_sigma) r_sigma(s+2)] = rotg( r_sigma(s+2), r_sigma(s+3) );
%
% Update projected right-hand side
            phi_n(k_sigma)    =       cs(s+2,k_sigma) *phi_n1(k_sigma);
            phi_n1(k_sigma)   = -conj(sn(s+2,k_sigma))*phi_n1(k_sigma);
            cs(1:s+1,k_sigma) = cs(2:s+2,k_sigma); sn(1:s+1,k_sigma) = sn(2:s+2,k_sigma);
%
% Update vector:
            if ( abs(r_sigma(s+2)) < eps )
               resvec = resvec(1:iter,:);
               flag = 3;
               return
            end
            w = (Vtilde(:,k_sigma) - W(:,:,k_sigma)*r_sigma(1:s+1))/r_sigma(s+2);
            W(:,1:s,k_sigma) = W(:,2:s+1,k_sigma); W(:,s+1,k_sigma) = w;
%
% Compute solution:
            x(:,k_sigma) = x(:,k_sigma) + phi_n(k_sigma)*w;
            if trueres
               if funA
                  normr(k_sigma) = norm(b - feval( function_A,x(:,k_sigma),A )+sigma(k_sigma)*x(:,k_sigma));
               else
                  normr(k_sigma) = norm(b - A*x(:,k_sigma)+sigma(k_sigma)*x(:,k_sigma));
               end
            else
               normr(k_sigma) = abs(phi_n1(k_sigma))*sqrt(j+1);
            end
	    converged(k_sigma) = normr(k_sigma) < tolb;
            resvec(iter+1,k_sigma) = normr(k_sigma);
         end 
      end
      if ( max(normr) < tolb | iter == maxit )
         break
      end
   end
end

resvec = resvec(1:iter+1,:);
for k_sigma = 1:n_sigma
   if funA
      relres(k_sigma) = norm(b - feval( function_A,x(:,k_sigma),A ) + sigma(k_sigma)*x(:,k_sigma))/normb;
   else
      relres(k_sigma) = norm(b - A*x(:,k_sigma) + sigma(k_sigma)*x(:,k_sigma))/normb;
   end
end
if ( max(relres) < tol )
   flag = 0;
elseif ( iter == maxit )
   flag = 1;
else
   flag = 2;
end

return

function mu = compmu( t, s, angle )
%
mu = 0;
ns = norm(s);
nt = norm(t);
ts = t'*s;
rho = abs(ts/(nt*ns));
om=ts/(nt*nt);
if ( rho < angle )
   om = om*angle/rho;
end
if ( abs(om) > eps )
   mu = 1/om;
end
%
return

function [c,s,r] = rotg(a,b);
% 
% ROTG: construct and apply Givens rotation
% ROTG returns c,s,r such that
%        | c       s || x |   | r |
%        |           ||   | = |   |
%        |-conj(s) c || y | = | 0 |
%
% ROTG is a translation of the BLAS routine CROTG
%
if abs(a) < eps
   c = 0;
   s = 1;
   r = b;
else
   scale = abs(a) + abs(b);
   rho = scale*sqrt(abs(a/scale)^2 +  abs(b/scale)^2);
   alpha = a/abs(a);
   c = abs(a)/rho;
   s = alpha*conj(b)/rho;
   r = alpha*rho;
end
%
return
