function [x_s,iter, resnrm, ritzval] = msgmres(A,b,sigma,tol,maxit)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%

% Multi-shift GMRES

funA = 0;
if isa(A,'struct')
   funA = 1;
   if isfield(A,'name')
      function_A = A.name;
   else
      error('Use field A.name to specify function name for matrix-vector multiplication');
   end
end

n       = length(b); 
n_sigma = length(sigma);
x_s     = zeros(n,n_sigma);
resnrm  = zeros(maxit+1,n_sigma);

beta = norm(b)
resnrm(1,:) = beta;

v = b/beta;
V = zeros(n,maxit+1);
V(:,1) = v;
H = zeros(maxit+1,maxit);
converged = zeros(n_sigma,1);

for k = 1:maxit,  
   
%  Compute new basis vector
   if funA
      v = feval( function_A, v, A);
   else
      v = A*v;
   end
%
%  Orthogonalise the new basis vector 
   for j = 1 : k
      H(j,k) = dot(V(:,j),v);
      v = v - H(j,k) * V(:,j);
   end
   H(k+1,k) = norm(v);
   v = v/norm(v);
   V(:,k+1) = v;
%
%  Solve small system
   Hr = H(1:k+1,1:k);
   e1 = zeros(k+1,1); e1(1)=beta; 
   Ir = eye(k+1,k);
   for i_sigma = 1:n_sigma
       if ( ~converged(i_sigma) )
          Hr_s = Hr-sigma(i_sigma)*Ir;
          y_s =Hr_s\e1;
          resnrm(k+1,i_sigma) = norm(e1 - Hr_s*y_s );
          if resnrm(k+1,i_sigma) < tol*beta
             x_s(:,i_sigma) = V(:,1:k)*y_s;
             converged(i_sigma) = 1;
          end
       end 
   end
   %normr = max(resnrm(k+1,:)); 

   if converged
      break
   end 
end

iter = k;
resnrm = resnrm(1:iter+1,:);
ritzval = zeros(iter,n_sigma);
ei = eig(Hr(1:k,1:k));
for i_sigma = 1:n_sigma
   ritzval(:,i_sigma) = ei - sigma(i_sigma);
end

end
