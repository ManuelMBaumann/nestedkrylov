function [x_s,resnrm,bm] = msgmres_coll(A,b,sigma,x_s0,maxit,b0)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%

% Restarted multi-shift GMRES, required msgmresk.m



funA = 0;
if isa(A,'struct')
   funA = 1;
   if isfield(A,'name')
      function_A = A.name;
   else
      error('Use field A.name to specify function name for matrix-vector multiplication');
   end
end

n       = length(b); 
n_sigma = length(sigma);
x_s     = zeros(n,n_sigma);
resnrm  = zeros(1,n_sigma);
bm      = ones(1,n_sigma);

if funA
  r = b - feval( function_A, x_s0(:,end), A);
else
  r = b - A*x_s0(:,end);
end

beta = norm(r);

v = r/beta;
V = zeros(n,maxit);

H = zeros(maxit+1,maxit);

for k = 1:maxit,  
   
    V(:,k) = v;
    
%  Compute new basis vector
   if funA
      v = feval( function_A, v, A);
   else
      v = A*v;
   end
%
%  Orthogonalise the new basis vector 
   for j = 1 : k
      H(j,k) = dot(V(:,j),v);
      v = v - H(j,k) * V(:,j);
   end
   H(k+1,k) = norm(v);
   v = v/norm(v);
end

%% Solve shifted system
e1 = zeros(k+1,1); e1(1)=beta;

% Solve seed system
y = H\e1;
z = e1-H*y;
resnrm(end) = norm(z);

x_s(:,end) = x_s0(:,end)+V*y;

%  Solve rest
Ir = eye(k+1,k);
for i_sigma = n_sigma-1:-1:1

   H_s = H-sigma(i_sigma)*Ir;
    
   y_s = [H_s z]\(b0(i_sigma)*e1);
   bm(i_sigma) = y_s(end);
   resnrm(i_sigma) =  abs(bm(i_sigma))*resnrm(end);
   x_s(:,i_sigma) = x_s0(:,i_sigma) + V*y_s(1:end-1);
   
end

end