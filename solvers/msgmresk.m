function [x_s,iter, resvec] = msgmresk(A,b,sigma,max_outer_it, max_trun_it)

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%


n         = length(b); 
n_sigma   = length(sigma);

x_s       = zeros(n,n_sigma);
b0        = ones(1,n_sigma);
resvec    = zeros(max_outer_it+1,n_sigma);


beta        = norm(b);
resvec(1,:) = beta;

for ii = 1:max_outer_it
    
    [x_s,resnrm,b0] = msgmres_coll(A,b,sigma,x_s,max_trun_it,b0);
    resvec(ii+1,:)=resnrm;
    
end
iter = ii;

end