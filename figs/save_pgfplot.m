function save_pgfplot(iter,resvec,var_names,f_name)

Nom = length (var_names);
fid = fopen(['figs/', f_name, 'resvec.dat'],'w');

fprintf(fid, '%s', 'iters ');
for om = 1:Nom
	fprintf(fid, '%s ', var_names(om,:));
end
fprintf(fid, '\n'); 


for i = 1:iter
    fprintf(fid, '%i ', i);  
    for om = 1:Nom
        fprintf(fid, '%0.15f ', resvec(i,om));  
    end
    fprintf(fid, '\n');  
end

fclose(fid)

end