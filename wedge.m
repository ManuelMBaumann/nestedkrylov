%%
%
%  driver for Helmholtz equation
%
%%

%   The software is distributed without any warranty.
%
%   Manuel Baumann
%   Copyright (c) July 2014
%

clear all
close all
addpath('discretization/')
addpath('solvers/')
addpath('aux')
addpath('figs')

Title = 'Helmholtz Equation';
exper = 0;
c = ['b','g','r','c','m','y','k'];

%% Choose problem
problem = 5; % 1-6: Absorption, 7-12: Reflection

if ( problem > 6 )
   sommerfeld = 0;
   ref = problem - 6;
else
   sommerfeld = 1;
   ref = problem;
end

%% Default parameters
maxit = 1000;
tol = 1e-8;
im = sqrt(-1);
s = 4;
sh = 0.7-0.7*im;
inner = 200;
plotInner = 0;
outer_flag = 1;
inner_flag = 0;

sz = [7 11; 13 21; 25 41; 49 81; 97 161; 193 321];
dm = sz(ref,:);
im = sqrt(-1);
f_max = 2^(ref);
for j = 1:ref+1
   f(j) = 2^(j-1);
end

%% Load matrices
laplace = ['wl',num2str(ref)];
K = spconvert(load(laplace));
mass = ['wm',num2str(ref)];
M = spconvert(load(mass));
if ( sommerfeld )
   damp = ['wd',num2str(ref)];
   C = spconvert(load(damp));
else
   C = [];
end 
n = length(K);
rhs = zeros(n,1);
rhs((dm(1)+1)/2) = 1;
%%

contin = 1;
while contin

   exper = exper + 1;

   solver_parameters;

   t0 = cputime;
   [x, iter, resvec,relres,method] = precon_ms(K,C,M,rhs,tol,maxit,f,sh,s,inner,plotInner,...
       inner_flag,outer_flag);
   time = cputime-t0;

   disp(['Experiment ', num2str(exper)]);
   disp('====================');

   output;

end

%%  plot solution
plot_wedge(dm,x,f);

